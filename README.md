# pinentry-drmoose

This is a pinentry program for gpg which just launches another pinentry program, specified by `$PINENTRY_USER_DATA`.

&copy; 2024 @drmoose. GNU GPL v3 or later.

# How

`pinentry-drmoose` just launches some other pinentry program. It will use the value of `$PINENTRY_USER_DATA` as the absolute path to the other pinentry program to invoke, or fall back to the macro `PINENTRY_FALLBACK` if `$PINENTRY_USER_DATA` is unset or invalid.

For security reasons, `pinentry-drmoose` considers invalid any `$PINENTRY_USER_DATA` that is not owned by root or does not start with `/usr/bin/pinentry-`.

Compile with `PINENTRY_FALLBACK` set to whichever gui pinentry you use by default. This will be a filename starting with `/usr/bin/pinentry-`. If you're using gnome it's probably `/usr/bin/pinentry-gnome3`. If you're using KDE it's probably `/usr/bin/pinentry-qt5`.

If the pinentry program you want to invoke requires a `PINENTRY_USER_DATA` environment variable itself, you can put the value after the path to the program. So, if `PINENTRY_USER_DATA=/usr/bin/pinentry-foo bar` then `pinentry-foo` will be called with `PINENTRY_USER_DATA` set to `bar`.


```bash
gcc -o pinentry-drmoose pinentry-drmoose.c -DPINENTRY_FALLBACK=/usr/bin/pinentry-qt5
sudo install -Dm755 pinentry-drmoose /usr/bin/pinentry-drmoose

# Set pinentry-drmoose as your pinentry-program in gpg-agent.conf
touch ~/.gnupg/gpg-agent.conf
sed -i_ '/pinentry-program/d' ~/.gnupg/gpg-agent.conf
echo "pinentry-program /usr/bin/pinentry-drmoose" >> ~/.gnupg/gpg-agent.conf

# Add this to your shell init (replace .bashrc with .zshrc if applicable)
cat >> ~/.bashrc <<'EOF'
if [ -t 0 ]; then
  export GPG_TTY=$(tty)
  export PINENTRY_USER_DATA=/usr/bin/pinentry-tty
  gpg-connect-agent updatestartuptty /bye >/dev/null
fi
EOF
```

# Why

GPG uses a "pinentry program" to prompt you for your passphrase. If you have all the relevant packages installed, it will default to popping up a qt window asking for your password.

I view anything which creates a new window while I'm using a terminal to be broken by design. I already have a perfect venue for interacting with software. Why make something appear on a completely different part of my screen that I then have to find, focus, and type into?

You can change this behavior by setting a `pinentry-program` in `~/.gnupg/gpg-agent.conf`, like for example

```
pinentry-program /usr/bin/pinentry-tty
```

But what about things like Firefox, which are not running in a terminal session but which need to be able to draw a GPG prompt? Well, what happens there is, they just time out.

Wouldn't it be nice if we could have a pinentry on the terminal if we're using a terminal, and the usual popup window if not? Apparently the authors of gnupg don't want us to do that.

Fortunately there's another mechanism we can use, `$PINENTRY_USER_DATA`, which is the only environment variable gpg passes through to the pinentry program. None of the default pinentries seem to do anything with this value, but a custom pinentry could.

So, this project lets you control which pinentry you get based on an environment variable.

# Developer Notes

```bash
clang-format -i \
  --style '{BasedOnStyle: LLVM, UseTab: ForIndentation, TabWidth: 2}' \
  --sort-includes \
  pinentry-drmoose.c
```
