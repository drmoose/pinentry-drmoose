/*
Copyright (c) 2024 Dan Charney (@drmoose)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef PINENTRY_FALLBACK
#define PINENTRY_FALLBACK / usr / bin / pinentry - qt5
#endif

#define STRINGIFY(X) #X
#define CALLFN(FN, ...) FN(__VA_ARGS__)
#define PINENTRY_FALLBACK_STR CALLFN(STRINGIFY, PINENTRY_FALLBACK)

#define PINENTRY_REQUIRED_PREFIX "/usr/bin/pinentry-"
#define PINENTRY_REQUIRED_PREFIX_LEN sizeof(PINENTRY_REQUIRED_PREFIX) - 1
#define MAX_PINENTRY_LEN 1024

/**
 * Given a $PINENTRY_USER_DATA value, verify that it begins with a valid path
 * to a safe pinentry program.
 *
 * The path to the program in pinentry_user_data may optionally be followed by
 * a space and a new value for the PINENTRY_USER_DATA environment variable to
 * be passed through to the desired program.
 *
 * Return value is zero if the supplied path string was not valid or is
 * insecure. If the entire string was the path, return value is -1. Otherwise,
 * the return value is an offset into the original pinentry_user_data after the
 * space delimiter.
 *
 * If the return value is nonzero, and trimmed_path_out is non-NULL,
 * *trimmed_path_out will be set to a newly-malloced string containing only the
 * path part of the pinentry_user_data, for passing on to execv.
 */
int validate_pinentry_user_data(char *pinentry_user_data,
                                char **trimmed_path_out) {
	if (NULL == pinentry_user_data) {
		return 0;
	}

	int out = 0;
	if (NULL != trimmed_path_out) {
		*trimmed_path_out = NULL;
	}

	/* if pinentry_user_data !~ /${PINENTRY_REQUIRED_PREFIX}[a-zA-Z0-9_\.-]*\0/
	 * return false */
	for (int i = 0; i < MAX_PINENTRY_LEN; ++i) {
		char c = pinentry_user_data[i];
		if (i < PINENTRY_REQUIRED_PREFIX_LEN) {
			if ('\0' == c) {
				return 0;
			}
		} else if ('\0' == c || ' ' == c) {
			out = '\0' == c ? -1 : i + 1;
			if (NULL != trimmed_path_out) {
				*trimmed_path_out = malloc(sizeof(char) * i);
				memcpy(*trimmed_path_out, pinentry_user_data, sizeof(char) * i);
				(*trimmed_path_out)[i] = '\0';
			}
			break;
		} else if (!(('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') ||
		             ('0' <= c && c <= '9') || c == '-' || c == '_' || c == '.')) {
			return 0;
		}
	}

	/* if ! -x path || owner(path) != 0 return false */
	struct stat sb;
	if (0 != stat(*trimmed_path_out, &sb)) {
		out = 0;
	} else if (!(sb.st_mode & S_IXUSR)) {
		out = 0;
	} else if (0 != sb.st_uid) {
		out = 0;
	}

	if (!out && NULL != trimmed_path_out) {
		free(*trimmed_path_out);
		*trimmed_path_out = NULL;
	}

	return out;
}

int main(int argc, char **argv) {
	char *pinentry_user_data = getenv("PINENTRY_USER_DATA");
	char *pinentry_program = NULL;
	int validation_result =
	    validate_pinentry_user_data(pinentry_user_data, &pinentry_program);
	if (!validation_result) {
		pinentry_program = strdup(PINENTRY_FALLBACK_STR);
		unsetenv("PINENTRY_USER_DATA");
	} else if (validation_result < 0) {
		unsetenv("PINENTRY_USER_DATA");
	} else {
		setenv("PINENTRY_USER_DATA", &(pinentry_user_data[validation_result]), 1);
	}

	/* execv's argv needs to be NULL-terminated so we need to memcpy the input
	 * argv to a new vector so we can add the NULL at the end.
	 */
	char **sub_argv = calloc(argc + 1, sizeof(char *));
	memcpy(sub_argv, argv, sizeof(char *) * argc);

	execv(pinentry_program, sub_argv);
	return 1; /* execv fell through */
}
